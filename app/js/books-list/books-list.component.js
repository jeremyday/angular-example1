app.component('booksList', {
  templateUrl: 'js/books-list/books-list.component.html',

  controller: ['$http', function BooksListController($http) {
    var self            = this;
    this.orderDirection = 1;
    this.orderProp      = 'title';

    $http.get('data/books.json')
         .then(function(response) {
           self.setBooks(response.data);
         });

    this.getAuthorSortIcon = function() {
      return getSortIcon('author');
    };

    this.getBooks = function() {
      if(this.filters == null || this.filters == undefined || R == null || R == undefined) {
        return this.books;
      }

      var titleFilter  = propFilter('title',  this.filters.title);
      var authorFilter = propFilter('author', this.filters.author);
      var genreFilter  = propFilter('genre',  this.filters.genre);
      var filter       = function(book) {
        return titleFilter(book) && authorFilter(book) && genreFilter(book);
      };

      return R.filter(filter, this.books);
    };

    this.getGenreSortIcon = function() {
      return getSortIcon('genre');
    }

    this.getOrderProp = function() {
      return (this.orderDirection == 1 ? '+' : '-') + this.orderProp;
    };

    this.getTitleSortIcon = function() {
      return getSortIcon('title');
    };

    this.navigateTo = function(event, book) {
      document.location.href = '#!/books/' + book.id;
    }

    this.onAuthorClick = function(event) {
      onClick('author', event);
    };

    this.onGenreClick = function(event) {
      onClick('genre', event);
    };

    this.onTitleClick = function(event) {
      onClick('title', event);
    };

    this.setBooks = function(books) {
      this.books = books;
    }

    function getSortIcon(prop) {
      if(self.orderProp != prop) {
        return '';
      }

      if(self.orderDirection == 1) {
        return 'fa-sort-desc';
      }

      return 'fa-sort-asc';
    }

    function onClick(prop) {
      event.stopPropagation();
      event.preventDefault();

      if(self.orderProp == prop) {
        self.orderDirection *= -1;
      } else {
        self.orderProp      = prop;
        self.orderDirection = 1;
      }
    }

    function propFilter(prop, filter) {
      var regexp = new RegExp(filter, 'i');

      return R.pipe(R.prop(prop), R.test(regexp));
    }
  }]
});
