app.component('bookDetail', {
  templateUrl: 'js/book-detail/book-detail.component.html',

  controller: ['$routeParams', '$http', function BookDetailController($routeParams, $http) {
    var self    = this;
    this.bookId = $routeParams.bookId;
    this.book   = {
      id:     null,
      title:  null,
      author: null,
      genre:  null
    };

    $http.get('data/books/' + this.bookId + '.json')
         .then(function(response) {
           self.setBook(response.data);
         });

    this.getBook = function() {
      return this.book;
    };

    this.getAuthor = function() {
      return this.getBook().author;
    };

    this.getGenre = function() {
      return this.getBook().genre;
    };

    this.getTitle = function() {
      return this.getBook().title;
    };

    this.saveBook = function(event) {
      event.stopPropagation();
      event.preventDefault();

      document.location.href = '#!/books';
    };

    this.setBook = function(book) {
      self.book = book;
    };
  }]
});
